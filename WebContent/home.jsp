<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
body {
	background-image: url('https://images.unsplash.com/photo-1593642632559-0c6d3fc62b89?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80');
	opacity:1;
	background-size:cover;
	background-repeat: none;
}
</style>
</head>
<body>
<div
		style="font-family: verdana; padding: 20px; border-radius: 10px; font-size: 20px; text-align: center;color:black;">
<a href="userDetails.jsp">+create</a>
<table border="2" align="center">
	<thead>
		<tr>
		    <th>SI No.</th>
			<th>Application Name</th>
			<th>Email</th>
			<th>User Name</th>
			<th>Email</th>
			<th>Password</th>
			
		</tr>
	</thead>
	<tbody>
 		<c:forEach items="${list}" var="app">
 			<tr>
 				<td>${app.id}</td>
 				<td>${app.appName}</td>
 				<td>${app.email}</td>
 				<td>${app.userName}</td>
 				<td>${app.password}</td>
            </td>
 			</tr>
 		</c:forEach>
	</tbody>
</table>
</div>

</table>
</body>
</html>