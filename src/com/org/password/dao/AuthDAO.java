package com.org.password.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.org.password.dto.LoginDTO;
import com.org.password.entity.Registers;
@Repository
public class AuthDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void saveRegisterDetails(Registers register) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(register);
			transaction.commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
	}
	
	public Registers getRegisterDataByEmailAndPwd(LoginDTO loginDTO) {
		Session session = sessionFactory.openSession();
		String hql="from Registers where email=:e and password=:p";
		Query query = session.createQuery(hql);
		query.setParameter("e", loginDTO.getEmail());
		query.setParameter("p", loginDTO.getPassword());
		Registers register = (Registers) query.uniqueResult();
		return register;
	}
}
