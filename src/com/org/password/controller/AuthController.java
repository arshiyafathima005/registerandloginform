package com.org.password.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.org.password.dto.LoginDTO;
import com.org.password.entity.AppUserDetails;
import com.org.password.entity.Registers;
import com.org.password.service.AppUserService;
import com.org.password.service.AuthService;
@Controller
@RequestMapping("/")
public class AuthController {
	@Autowired
	private AuthService authService;
	
	@Autowired
	private AppUserService appUserService;
	
	@RequestMapping("/saveRegisterDetails")
	public ModelAndView saveRegisterDetails(Registers  register) {
		authService.saveRegisterDetails(register);
		return new ModelAndView("login.jsp","msg","Registraion successfull please login!");
	}
	@RequestMapping("/login")
	public ModelAndView login(LoginDTO loginDTO,HttpServletRequest request) {
		Registers register = authService.getRegisterDataByEmailAndPwd(loginDTO);
		if(register != null) {
			HttpSession session = request.getSession();
			session.setAttribute("register", register);
			List<AppUserDetails> list = appUserService.getAppdetailsByUserId(register.getId());
			list.forEach(a->{
				System.out.println(a);
			});
			return new ModelAndView("home.jsp","list",list);
		}
		return new ModelAndView("login.jsp","msg","Invalid credentials!!");
	}

	
}
