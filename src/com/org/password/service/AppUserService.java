package com.org.password.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.password.dao.AppUserDAO;
import com.org.password.entity.AppUserDetails;
@Service
public class AppUserService {
	@Autowired
	private AppUserDAO appUserDAO;
	
	public AppUserService() {
		System.out.println(this.getClass().getSimpleName()  +" created");
	}
	
	public void saveUserData(AppUserDetails appUserDetails) {
		appUserDAO.saveUserData(appUserDetails);
	}
	public List<AppUserDetails> getAppdetailsByUserId(Long id) {
		return appUserDAO.getAppdetailsByUserId(id);
	}
}
