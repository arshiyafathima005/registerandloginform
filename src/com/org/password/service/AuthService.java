package com.org.password.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.password.dao.AuthDAO;
import com.org.password.dto.LoginDTO;
import com.org.password.entity.Registers;
@Service
public class AuthService {
	@Autowired
	private AuthDAO authDAO;
	
	public void saveRegisterDetails(Registers register) {
		authDAO.saveRegisterDetails(register);
	}
	
	public Registers getRegisterDataByEmailAndPwd(LoginDTO loginDTO) {
		return authDAO.getRegisterDataByEmailAndPwd(loginDTO);
	}
	
	
}
